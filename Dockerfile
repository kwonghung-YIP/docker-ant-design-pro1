FROM node:carbon

RUN cd /root; \
    git clone https://gitlab.com/kwonghung.yip/ant-design-pro-app1.git myapp1; \
	cd myapp1; \
	npm install

WORKDIR /root/myapp1
